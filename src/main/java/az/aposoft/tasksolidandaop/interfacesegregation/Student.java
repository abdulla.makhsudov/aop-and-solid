package az.aposoft.tasksolidandaop.interfacesegregation;

public class Student implements Human{
    @Override
    public void setName(String name) {

    }

    @Override
    public void showAge() {

    }

    // Can not be implemented. Fixed way of separate this method other inteface and Student doesn't implement that interface
    @Override
    public void increaseSalary(double salary) {

    }
}
