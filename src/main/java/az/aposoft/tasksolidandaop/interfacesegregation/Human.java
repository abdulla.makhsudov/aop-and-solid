package az.aposoft.tasksolidandaop.interfacesegregation;

public interface Human {
    void setName(String name);
    void showAge();
    void increaseSalary(double salary);
}
