package az.aposoft.tasksolidandaop.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Aspect
@Configuration
@Slf4j
public class LoggingAspect {

    @Around("@annotation(az.aposoft.tasksolidandaop.annotation.LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("Enter: {} - {}()", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName());
        long startTime = System.nanoTime();
        Object result = joinPoint.proceed();
        long stopTime = System.nanoTime();
        log.info("Exit: {} - {}()", TimeUnit.NANOSECONDS.toMillis(stopTime - startTime) +
                " | " + joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        return result;
    }
}
