package az.aposoft.tasksolidandaop.lisvoksubstitution;

public interface Human {
    void setName(String name);
    void showAge();
    void increaseSalary(double salary);
}
