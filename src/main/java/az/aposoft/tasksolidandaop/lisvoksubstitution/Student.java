package az.aposoft.tasksolidandaop.lisvoksubstitution;

public class Student implements Human{

    @Override
    public void setName(String name) {

    }

    @Override
    public void showAge() {

    }

    @Override
    public void increaseSalary(double salary) {
        // Student hasn't salary therefore it can't implement this method and Student can't replacement Human
    }
}
