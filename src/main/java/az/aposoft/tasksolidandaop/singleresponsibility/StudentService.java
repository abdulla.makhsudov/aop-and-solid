package az.aposoft.tasksolidandaop.singleresponsibility;

import org.springframework.stereotype.Service;

import java.util.List;


public class StudentService {

    public List<Student> getAllStudent() {
        return List.of();
    }

    public void deleteStudent(Long id) {}

    // this method could separated here and moved in user class
    public void checkUserBalance() {}

    // also this method could separated here and moved in user class
    public void getAllUser() {}

}
