package az.aposoft.tasksolidandaop.singleresponsibility;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class Student {
    private String name;
    private String surname;
}
