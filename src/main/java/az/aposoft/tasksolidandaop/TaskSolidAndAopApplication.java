package az.aposoft.tasksolidandaop;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class TaskSolidAndAopApplication implements CommandLineRunner {

    private final StartExecTime startExecTime;

    public static void main(String[] args) {
        SpringApplication.run(TaskSolidAndAopApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        startExecTime.startLogExecTime();
    }
}
