package az.aposoft.tasksolidandaop.openclosedprinciples;

public interface CharacterCount {
    void sumNameCharacter(String name);
    void sumNameAndSurnameCharacter(String name, String surname);
}
