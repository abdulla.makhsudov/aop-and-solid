package az.aposoft.tasksolidandaop.openclosedprinciples;

import org.springframework.stereotype.Service;


public class StudentService implements CharacterCount{

    public void sumStudentAge() {}

    // this method would leave here and moved into interface
    public void sumStudentNameCharacter(String name) {}

    @Override
    public void sumNameCharacter(String name) {

    }

    // as well as this method would moved into interface
    public void sumStudentNameAndSurnameCharacter(String name, String surname) {}

    @Override
    public void sumNameAndSurnameCharacter(String name, String surname) {

    }
}
