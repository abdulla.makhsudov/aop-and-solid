package az.aposoft.tasksolidandaop;

import az.aposoft.tasksolidandaop.annotation.LogExecutionTime;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

@Component
public class StartExecTime {

    private String describe;
    private String finish;

    @SneakyThrows
    @LogExecutionTime
    public void startLogExecTime() {
        describe = "Hi I'm the started method for execution time";
        System.out.println(describe);
        Thread.sleep(1000);
        finish = "Finally I'm stoped my work ))";
        System.out.println(finish);
    }
}
