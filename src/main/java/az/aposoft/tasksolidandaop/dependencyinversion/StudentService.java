package az.aposoft.tasksolidandaop.dependencyinversion;

public class StudentService implements IServiceImpl{

    // this class depend on concret class. fix way is to abstract this class
    private ServiceImpl serviceImpl;

    public StudentService(ServiceImpl serviceImpl) {
        this.serviceImpl = serviceImpl;
    }

    public void getStudent() {
        serviceImpl.getStudent();
    }

    @Override
    public void IgetStudent() {

    }
}
